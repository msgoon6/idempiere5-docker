# iDempiere5 Docker

By msgoon6

Debian Install

1. Load image into docker host: 
    gzcat idempiere-docker-pgsql-5.1.0.latest.tar.gz | docker load
    gzcat idempiere-docker-server-5.1.0.latest.tar.gz | docker load
2. Create data volume: 
    docker volume create --name idempiere-pgsql-datastore
3. Run idempiere pgsql: 
    docker run -d --name="idempiere-pgsql" -v idempiere-pgsql-datastore:/data -p 5432:5432 -e PASS="postgres" idempiere/idempiere-docker-pgsql:5.1.0.latest
    docker logs -f idempiere-pgsql #check data preparation status
4. Run idempiere server: 
    docker run -d -t --link idempiere-pgsql:idempiere-db --name="idempiere-server" -p 80:8080 -p 443:8443 idempiere/idempiere-docker-server:5.1.0.latest
    docker logs -f idempiere-server #check server status

CentOS Install

1. Load image into docker host: 
    zcat idempiere-docker-pgsql-5.1.0.latest.tar.gz | docker load
    zcat idempiere-docker-server-5.1.0.latest.tar.gz | docker load
2. Create data volume: 
    docker volume create --name idempiere-pgsql-datastore
3. Run idempiere pgsql: 
    docker run -d --name="idempiere-pgsql" -v idempiere-pgsql-datastore:/data -p 5432:5432 -e PASS="postgres" idempiere/idempiere-docker-pgsql:5.1.0.latest
    docker logs -f idempiere-pgsql #check data preparation status
4. Run idempiere server: 
    docker run -d -t --link idempiere-pgsql:idempiere-db --name="idempiere-server" -p 80:8080 -p 443:8443 idempiere/idempiere-docker-server:5.1.0.latest
    docker logs -f idempiere-server #check server status